import {qs, $on, $delegate} from './helpers';
import Template from './template';

const _itemId = element => element.dataset.id || element.parentNode.dataset.id || element.parentNode.parentNode.dataset.id;
const _itemTitle = element => element.dataset.title || element.parentNode.dataset.title || element.parentNode.parentNode.dataset.title;
const _itemPrice = element => parseFloat((element.dataset.price || element.parentNode.dataset.price || element.parentNode.parentNode.dataset.price).replace(',','.'));


export default class View {
	/**
	 * @param {!Template} template A Template instance
	 */
	constructor(template) {
		this.template = template;
		this.$menuList = qs('.ys-list');
		this.$titleList = qs('.ys-titles');
		this.$basketListMain = qs('.ys-basket');
		this.$basketList = qs('.basket-list');
		this.$basketTotal = qs('.ys-basket-total-area');
		this.$restaurantDetail = qs('.restaurant-detail');
		this.$searchProduct = qs('.search-product');
	}
	
	
	/**
	 * Change the appearance of the filter buttons based on the route.
	 *
	 * @param {string} route The current route
	 */
	updateHeader(route) {
		qs('.nav .active').className = 'nav-link';
		qs(`.nav [href="#/${route}"]`).className = 'nav-link active';
	}
	
	
	/**
	 * Search product, function bind to Controller.
	 * @param handler Function called on synthetic event.
	 */
	bindFoodSearch(handler) {
		$on(this.$searchProduct, 'input', ({target}) => {
			const title = target.value.trim();
			handler(title);
		});
	}
	
	/**
	 * Static function for fix duplicate code.
	 * Get arguments on target then send to handler.
	 * @param target Event Object.
	 * @param handler Function called on synthetic event.
	 */
	static addItemFunction(target, handler) {
		let countNode = qs('input', target.parentNode);
		let count = parseInt(countNode.value) === 0 ? 1 : parseInt(countNode.value);
		handler(_itemId(target), _itemTitle(target), _itemPrice(target), count);
		countNode.value = count;
	}
	
	/**
	 * Add product to basket, function bind to Controller.
	 * @param handler Function called on synthetic event.
	 */
	bindItemAddBasket(handler) {
			$delegate(this.$menuList, '.products-list:not(.unavailable) .add-product' ,'click', event  => {
				View.addItemFunction(event.target, handler);
			});
	}
	
	/**
	 * Change product count on basket, function bind to Controller.
	 * @param handler Function called on synthetic event.
	 */
	bindItemAddBasketCount(handler) {
		$delegate(this.$basketList, '.basket-count input' ,'blur', event  => {
			View.addItemFunction(event.target, handler);
		}, true);
	}
	
	/**
	 * Products add from the view
	 * @param items [Array] of objects
	 */
	showItems(items) {
		this.$menuList.innerHTML = this.template.menuList(items);
	}
	
	/**
	 * Products of titles add from the view
	 * @param items [Array] of objects
	 */
	showHeader(items) {
		this.$titleList.innerHTML = this.template.titleList(items);
	}
	
	
	/**
	 * Basket of items add from the view
	 * Change basket title to restaurant's name
	 * @param items [Array] of objects
	 */
	showBasket(items) {
		this.$basketList.innerHTML = this.template.basketList(items);
	}
	
	/**
	 * restaurant's detail of items add from the view
	 * Change document title to restaurant's name
	 * @param items [Array] of objects
	 */
	showRestaurantDetail(items) {
		qs('.store').innerHTML = items.DisplayName;
		document.title = items.DisplayName;
		this.$restaurantDetail.innerHTML = this.template.restaurantDetail(items);
	}
	
	/**
	 * Calculate basket of items, add from the view
	 * Control for Basket items count
	 * @param items [Array] of objects
	 */
	calculateTotal(items) {
		let total = items.reduce((acc, item) => {
			return acc + (item.price * item.count);
		}, 0);
		if (total) {
			return this.$basketTotal.innerHTML = `<p>Total:</p> <span class="ys-basket-total">${total}</span> TL`;
		}
		this.$basketTotal.innerHTML = `<div class="ys-basket-empty"></div><p>Sepetiniz henüz boş.</p>`;
	}
	
	
	/**
	 * Remove a item on basket , function bind to Controller.
	 * @param handler Function called on synthetic event.
	 */
	bindRemoveItemBasket(handler) {
		$delegate(this.$basketList, '.destroy', 'click', ({target}) => {
			handler(_itemId(target));
		});
	}
	
	/**
	 * Remove all items on basket, function bind to Controller.
	 * @param handler Function called on synthetic event.
	 */
	bindRemoveAllBasket(handler) {
		$delegate(this.$basketListMain, '.ys-remove-all-basket', 'click', () => {
			handler();
		});
	}
	
	/**
	 * Scroll to categoryId with id, function bind to Controller.
	 * @param handler Function called on synthetic event.
	 */
	bindGoMenu(handler) {
		$delegate(this.$titleList, '.title-list-item a', 'click', ({target}) => {
			this.hideOtherComponents('');
			window.location = '#/';
			setTimeout(()=> {
				let top = qs(`[data-categoryId="${_itemId(target)}"]`).getBoundingClientRect().top;
				handler(top);
			}, 0);
		});
	}
	
	/**
	 * Route for tabs
	 * @param route String.
	 */
	hideOtherComponents(route) {
		this.$titleList.classList.remove('d-none');
		this.$menuList.classList.remove('d-none');
		if (route === '') {
			this.$titleList.classList.add('d-none');
		}
		if (route === 'titles') {
			this.$menuList.classList.add('d-none');
		}
	}
	
}