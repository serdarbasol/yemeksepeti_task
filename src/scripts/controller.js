import Store from './store';
import View from './view';
import Data from '../menuData.json';
import restaurantData from '../restoranData.json';

export default class Controller {
	/**
	 * @param  {!Store} store A Store instance
	 * @param  {!View} view A View instance
	 */
	constructor(store, view) {
		this.store = store;
		this.view = view;
		this.data = Data.d.ResultSet;
		this.restaurantData = restaurantData.d.ResultSet;
		Controller.calculateAllOf = Controller.calculateAllOf.bind(this);
		view.bindFoodSearch(this.productSearch.bind(this));
		view.bindItemAddBasket(this.itemAddBasket.bind(this));
		view.bindItemAddBasketCount(this.itemAddBasketCount.bind(this));
		view.bindRemoveItemBasket(this.removeItemBasket.bind(this));
		view.bindRemoveAllBasket(this.removeAllBasket.bind(this));
		view.bindGoMenu(Controller.goMenu.bind(this));
		
		this._activeRoute = '';
	}
	
	
	/**
	 * Set and render the active route.
	 *
	 * @param {string} raw '' | '#/' | '#/active' | '#/completed'
	 */
	setView(raw) {
		const route = raw.replace(/^#\//, '');
		this._activeRoute = route;
		this._filter();
		this.view.updateHeader(route);
	}
	
	/**
	 * Refresh the list based on the current route.
	 *
	 * @param {boolean} [force] Force a re-paint of the list
	 */
	_filter() {
		const route = this._activeRoute;
		if (route === 'titles') {
			this.setHeader();
			this.view.hideOtherComponents(route);
		}
		if (route === '') {
			this.view.hideOtherComponents(route);
		}
		this.setMenuData();
		this.view.showRestaurantDetail(this.restaurantData);
	}
	
	/**
	 * Static function for calculate function should'nt be duplicate
	 * @param force get local storage, for once get local storage
	 * @param data is callback store event storage data
	 */
	static calculateAllOf(force, data) {
		let store;
		if (force) {
		 store = this.store.getLocalStorage('ys-store');
		}
		this.view.showBasket(store || data);
		this.view.calculateTotal(store || data);
	}
	
	/**
	 * Add an Item to the Store and display it in the list.
	 *
	 * @param {!string} title Title of the new item
	 */
	itemAddBasket(id, title, price, count) {
		this.store.insert({
			id,
			title,
			price,
			count
		}, (data) => {
			Controller.calculateAllOf(false, data);
		}, false);
	}
	
	itemAddBasketCount(id, title, price, count) {
		this.store.insert({
			id,
			title,
			price,
			count
		}, (data) => {
			Controller.calculateAllOf(false, data);
		}, true);
	}
	
	/**
	 * Remove the data and elements related to an Item.
	 *
	 * @param {!number} id Item ID of item to remove
	 */
	removeItemBasket(id) {
		this.store.remove({id}, (data) => {
			Controller.calculateAllOf(false, data);
		});
	}
	
	/**
	 * Remove the data and elements related to an Item.
	 *
	 * @param {!number} remove all items
	 */
	removeAllBasket() {
		this.store.removeAll(() => {
			Controller.calculateAllOf(false, []);
		});
	}
	
	/**
	 * Save an Item in edit.
	 *
	 * @param {number} id ID of the Item in edit
	 * @param {!string} title New title for the Item in edit
	 */
	productSearch(title) {
		const result = JSON.parse(JSON.stringify(this.data));
		for (let i = result.length - 1; i >= 0; i--) {
			for (let j = result[i].Products.length - 1; j >= 0; j--) {
				const name = result[i].Products[j].DisplayName;
				if (name.toLowerCase().indexOf(title.toLowerCase()) === -1) {
					result[i].Products.splice(j, 1);
				}
			}
			if (result[i].Products.length === 0) {
				result.splice(i, 1);
			}
		}
		this.view.showItems(result);
	}
	
	static goMenu(top) {
		window.scroll(0, top - 65);
	}
	
	/**
	 * Set and render the active route.
	 *
	 * @param {string} raw '' | '#/' | '#/active' | '#/completed'
	 */
	setMenuData() {
		this.view.showItems(this.data);
		Controller.calculateAllOf(true);
	}
	
	setHeader() {
		const headerNames = this.data.map(item => {
			return {
				name: item.CategoryDisplayName,
				id: item.CategoryName
			};
		});
		this.view.showHeader(headerNames);
	}
	
}