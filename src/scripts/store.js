export default class Store {
	/**
	 * @param {!string} name Database name
	 * @param {function()} [callback] Called when the Store is ready
	 */
	constructor(name, callback) {
		/**
		 * @type {Storage}
		 */
		const localStorage = window.localStorage;
		
		
		/**
		 * @type {basketList}
		 */
		let basketList;
		
		/**
		 * Read the local basketList from localStorage.
		 *
		 * @returns {basketList} Current array of basketList
		 */
		this.getLocalStorage = (name) => {
			return basketList || JSON.parse(localStorage.getItem(name) || '[]');
		};
		
		/**
		 * Write the local basketList to localStorage.
		 *
		 * @param {basketList} items Array of items to write
		 */
		this.setLocalStorage = (items) => {
			localStorage.setItem(name, JSON.stringify(basketList = items));
		};
		
		if (callback) {
			callback();
		}
	}
	
	/**
	 * Remove items from the Store based on a query.
	 *
	 * @param {query} query Query matching the items to remove
	 * @param {function(callback)|function()} [callback] Called when records matching query are removed
	 */
	remove(query, callback) {
		let k;
		
		const basketList = this.getLocalStorage().filter(items => {
			for (k in query) {
				if (query[k] !== items[k]) {
					return true;
				}
			}
			return false;
		});
		
		this.setLocalStorage(basketList);
		
		if (callback) {
			callback(basketList);
		}
	}
	
	/**
	 * Remove items from the Store based on a query.
	 *
	 * @param {function(callback)|function()} [callback] Called when records matching query are removed
	 */
	removeAll(callback) {
		this.setLocalStorage([]);
		
		if (callback) {
			callback();
		}
	}
	
	/**
	 * Insert an item into the Store.
	 *
	 * @param {item} item Item to insert
	 * @param {function()} [callback] Called when item is inserted
	 */
	insert(item, callback, force) {
		let todos = this.getLocalStorage();
		let i = 0;
		for (i; i < todos.length; i++) {
			if (todos[i].id === item.id) {
				if (force) {
					todos[i].count = item.count;
					break;
				}
				item.count += todos[i].count;
				delete todos[i];
				break;
			}
		}
		todos[i] = item;

		this.setLocalStorage(todos);

		if (callback) {
			callback(todos);
		}
	}
	
}