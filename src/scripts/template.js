function menuProducts(product) {
	return product.reduce((a, item) => a + `
		<li class='${['products-list list-group-item', item.ProductIsOpen === 'Acik' ? 'available' : 'unavailable'].join(' ')}'
			data-id='${item.ProductId}'
			data-title='${item.DisplayName}'
			data-price='${item.ListPrice}'>
			<div class="product-actions">
				<input class="menu-count" type="text" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\\..*)\\./g, '$1');" value="1">
				<button class="add-product">+</button>
			</div>
		  <div class="product">
		      <div class="product-info">
	          <a>${item.DisplayName}</a>
	          <span class="unavailable-info">- Şu anda mevcut değil.</span>
				</div>
		      <div class="product-desc">
		        ${item.Description !== '...' ? item.Description : ''}
				</div>
	    </div>
	    <div class="product-price">
				<span class="price">${item.ListPrice} TL</span>
			</div>
		</li>`, '');
};

export default class Template {
	
	/**
	 * Format the contents of a todo list.
	 *
	 * @param {ItemList} items Object containing keys you want to find in the template to replace.
	 * @returns {Array} Contents for a todo list
	 *
	 * @example
	 * view.show({
	 *	id: 1,
	 *	title: "Hello World",
	 *	completed: false,
	 * })
	 */
	menuList(items) {
		return items.reduce((a, item) => a + `
			<div class="card mb-3">
				<div class="card-header" data-categoryId="${item.CategoryName}">
					${item.DisplayName}
				</div>
				<div class="card-body">
					<ul class="list-group list-group-flush">
						${menuProducts(item.Products)}
					</ul>
				</div>
			</div>`, '');
	}
	
	titleList(items) {
		return `<ul class="title-list">
			${items.reduce((a,item) => a + `
				<li class="title-list-item" data-id="${item.id}">
					<a>${item.name}</a>
				</li>
			`, '')}
		</ul>`;
	}
	
	basketList(items) {
		return `
			<ul>${items.reduce((a, item) => a + `
				<li data-id='${item.id}'
					data-title='${item.title}'
					data-price=${item.price}>
					<span class="title">${item.title}</span>
					<div class="basket-count">
						<input type="number" value=${item.count} oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\\..*)\\./g, '$1');">
					</div>
					<span class="price">${item.price * item.count}TL</span>
					<a class="destroy"></a>
				</li>`, '')}
			</ul>`;
	}
	
	restaurantDetail(item) {
		return `
			<div class="restaurant-detail-inner">
				<div class="points-area">
					<p>Restoran Puanları</p>
					<div class="points">
						<div class="point">
							<span class="title">Hız</span>
							<span class="number">${item.Speed}</span>
						</div>
						<div class="point">
							<span class="title">Servis</span>
							<span class="number">${item.Serving}</span>
						</div>
						<div class="point">
							<span class="title">Lezzet</span>
							<span class="number">${item.Flavour}</span>
						</div>
					</div>
				</div>
				<div class="delivery-info">
					<div class="info">
						<div class="money"></div>
						<div class="title">
							Minimum Paket Tutarı
						</div>
						<p class="price">${item.DeliveryFee} TL</p>
					</div>
					<div class="info">
						<div class="clock"></div>
						<div class="title">
							Çalışma Saatleri (bugün)
						</div>
						<p>${item.WorkingHours.reduce((a,time)=> {
								if (time.IsToday) {
									a = time.WorkingHours[0];
								}
								return a;
							}, '')}
						</p>
					</div>
					<div class="info">
						<div class="bike"></div>
						<div class="title">
							Servis Süresi (ortalama)
						</div>
						<p>${item.DeliveryTime} dk.</p>
					</div>
				</div>
				<div class="restaurant-img">
					<img src="https://cdn.yemeksepeti.com/CategoryImages/TR_ISTANBUL/churrella_big.gif" alt="">
				</div>
			</div>`;
	}
	
}